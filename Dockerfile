FROM devopsworks/golang-upx:1.22 as builder

ENV GO111MODULE=on

ARG version
ARG builddate

WORKDIR /go/src/gitlab.com/devopsworks/tools/influxevent/

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# hadolint ignore=SC2016
RUN GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0 \
    go build \
    -tags release \
    -ldflags '-w -extldflags "-static" -X main.Version=${version} -X main.BuildDate=${builddate}' -a \
    -o /go/bin/influxevent && \
    strip /go/bin/influxevent && \
    /usr/local/bin/upx -9 /go/bin/influxevent

# hadolint ignore=DL3006
FROM gcr.io/distroless/base

COPY --from=builder /go/bin/influxevent /usr/local/bin/influxevent

ENTRYPOINT ["/usr/local/bin/influxevent"]