module gitlab.com/devopsworks/tools/influxevent

go 1.22

require (
	github.com/crgimenes/goconfig v1.2.1
	github.com/mitchellh/go-ps v1.0.0
	github.com/struCoder/pidusage v0.1.3
)

require gopkg.in/yaml.v2 v2.2.8 // indirect
