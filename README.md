# Influxevent

[![build status](https://gitlab.com/devopsworks/tools/influxevent/badges/master/pipeline.svg)](https://gitlab.com/devopsworks/tools/influxevent/commits/master)
[![coverage report](https://gitlab.com/devopsworks/tools/influxevent/badges/master/coverage.svg)](https://gitlab.com/devopsworks/tools/influxevent/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/github.com/devops-works/influxevent)](https://goreportcard.com/report/github.com/devops-works/influxevent)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Influxevent wraps commands and sends command result & timing to an influxDB
server.

The influx entries contains the following values:

- *command duration* (field `duration`, label `_type=end`)
- *command exit value* (field `status`, `_type=end`)
- *command cpu usage* for per sample period (field `cpu`, label `_type=metric`)
- *command memory usage* for per sample period (field `memory`, label `_type=metric`)

When invoking `influxevent`, you can pass:

- the measurement name (e.g. `cron`)
- a list of tag names and values (`tag1=val1,tag2=val2`); `host` tag is added
  automatically unless it is passed in `-influx_tags`

Influxevent transparently proxies command's stdout/stderr and exit value.

## Usage

### Quick start

Using in a cron job:

```bash
0 1 * * * influxevent -timeout 300 -influx_server https://influx.example.com:8086/ -influx_db mydb -measurement cron -period 100 -influx_tag command=backup -influx_retry 3 -- /usr/local/bin/database_backup >> /var/log/backups.log 2>&1
```

then in influxdb:

```bash
> select * from cron
name: cron
time                           command cpu duration    etype  host  memory status
----                           ------- --- --------    -----  ----  ------ ------
2020-05-10T22:51:05.390134036Z backup      10.06035404 event  host1        0
2020-05-10T22:51:05.491496489Z backup  10              metric host1 733184
2020-05-10T22:51:05.591504518Z backup  12              metric host1 733184
2020-05-10T22:51:05.691497143Z backup  11              metric host1 733184
2020-05-10T22:51:05.791500623Z backup  15              metric host1 733184
...
>
```

Adding tags:

```bash
0 1 * * * influxevent -influx_server https://influx.example.com:8086/ -influx_db mydb -influx_measurement events -influx_tags program=database_backup,db=foodb -- /usr/local/bin/database_backup foodb >> /var/log/backups.log 2>&1
```

### Arguments

General invocation: `influxevent [options] -- cmd args...`

Options:

- `-influx_url` (`$INFLUX_URL`): influxdb server URL (no events are send if not set)
- `-influx_db` (`$INFLUX_DB`): influxdb database (no events are send if not set)
- `-influx_user` (`$INFLUX_USER`): influxdb username (default: none)
- `-influx_pass` (`$INFLUX_PASS`): influxdb password (default: none)
- `-influx_measurement` (`$INFLUX_MEASUREMENT`): influxdb measurement (default:
  none, required when server is set)
- `-influx_tags` (`$INFLUX_TAGS`): comma-separated k=v pairs of influxdb tags
  (default: none, example: 'foo=bar,fizz=buzz')
- `-influx_retries` (`$INFLUX_RETRIES`): how many times we retry to send the
  event to influxdb(default: 3) (default 3)
- `-influx_timeout` (`$INFLUX_TIMEOUT`): timeout writing to influxdb in ms
  (default: 5000)
- `-influx_dryrun` (`$INFLUX_DRYRUN`): influxdb dry run (runs command and dumps
  influx datapoints instead of sending them to influxdb)
- `-lock_file` (`$LOCK_FILE`): lock file location (default: no locking)
- `-lock_duration` (`$LOCK_FILE`): lock file TTL (default: none)
- `-lock_exit_value` (`$LOCK_EXIT_VALUE`): exit value to use when lock file
  prevents execution (default: 123)
- `-timeout` (`$TIMEOUT`): command timeout (default: 0, no timeout)
- `-period` (`$PERIOD`): process consumption sample period (ms)
- `-verbose` (`$VERBOSE`): verbose execution
- `-version`: shows version

You can leverage environment variables in crontabs to have shorter cron
definitions. For instance:

```
INFLUX_URL=http://1.2.3.4:8086
INFLUX_DB=mydb
INFLUX_USER=user
INFLUX_PASS=pass
INFLUX_MEASUREMENT=cron
PERIOD=100

# Database Backup
0 1 * * * influxevent -influx_tags "type=backup,engine=mysql,db=customers" -- /usr/local/bin/database_backup >> /var/log/backups.log 2>&1
0 6 * * 7 influxevent -influx_tags "type=certbot,domain=example.org" -- /usr/bin/certbot certonly -n -d example.com -d www.example.com >> /var/log/certbot.log 2>&1
```

## Tags & metrics

`influxevent` returns metrics with several tags and values. The tags inserted
internally (except`_host`) will be prefixed by `_` to avoid collision with
user-supplied tags.

### Tags

- all the tags passed as parameters (deduplicated)
- `_type`: the type of information carried
   - `start`: event for the start of the executed command
   - `end`: event for the end of the executed command
   - `metric`: metric generated every period (see `-period` above)
- `_status`: either `error` or `ok` depending on exit value
- `host`: hostname where influxevent is executed

### Values

Values passed depend on the `_type` tag:

- `start` contains:
   - command `duration` duration in ms
   - command `status` (exit value)
- `end` contains:
   - command `duration` duration in ms
   - command `status` (exit value)
- `metric`: contains:
   - `cpu` usage for sample
   - `memory` usage for sample

## Locking

`influxevent` offers a locking mechanism (àla `flock(1)`) to prevent concurrent
execution. This can be useful when running non-concurrent commands with
varying execution times (e.g. backups over the network).

The `lock_file` flag tells influxevent where to put the lockfile. You must use
a full filename. This way, you control which executions are controlled by the
file.

If an execution is attempted and the lock file is already present,
`influxevent` will exit with `lock_exit_value` exit value.

While lock file removal should be handled properly even in case of termination
signals, mishaps can still happen (bug in the code, machine powered down by
pulling the cord, ...).

Thus, `influxevent` will check if the pid marked inside the lock file is still
running. If yes, it will bailout, if no, it will take control of the lock file
and process to run.

You can also ask influxevent to remove an existing lock file if it is older than
`lock_duration` if you wish (effectively forcing it to run the command).

However the best way is to monitor executions in InfluxDB in a dead-man switch
manner and act accordingly. For instance, if you expect 1 backup every hour and
you have only 20 in the last 24h, it might be good to sound an alarm.

For instance:

```bash
$ ./bin/influxevent -lock_file /tmp/sleep -- sleep 20 &
[1] 1708
2024/04/15 23:09:38 using lock file /tmp/sleep
$ ./bin/influxevent -lock_file /tmp/sleep -- sleep 20
2024/04/15 23:09:40 using lock file /tmp/sleep
2024/04/15 23:09:40 error handling lockfile: lock file /tmp/sleep exists, exiting
```

The basic pseudo-code for lock file handling is:

```
is there a pid lock-file ?
  no: create one and proceed
  yes: is the owning process still alive ?
    no: take control of lock file and proceed
    yes: is lock_duration set and expired ?
      yes: take control of lock file and proceed
      no: exit complaining about the lockfile being held
```

## Installing

### Binary

```bash
# YOLO
curl -sL https://gitlab.com/devopsworks/tools/influxevent/uploads/b8698cf8c1509e06c649547b51d6ebb5/influxevent-amd64-v0.11.gz -o - | gunzip > influxevent
chmod +x influxevent
sudo mv influxevent /usr/local/bin/influxevent
```

or if you have Go installed:

```bash
go install github.com/devops-works/influxevent
```

or if you have [Binenv](https://github.com/devops-works/binenv) installed:

```bash
# update if needed
# binenv update -f influxevent
# then
binenv install influxevent
```

### Docker

Not recommended, but possible:

```bash
docker run -v path_to_binary:/usr/bin/binary registry.gitlab.com/devopsworks/tools/influxevent:v0.7 -- /usr/bin/binary
```

e.g.:

```bash
docker run -v /usr/bin/sleep:/bin/sleep registry.gitlab.com/devopsworks/tools/influxevent:v0.7 -- sleep 1
```

### Compiling

```bash
make
```

## Contributions

Welcomed !

## Licence

WTFPL
