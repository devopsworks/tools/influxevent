package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

func Test_main(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			main()
		})
	}
}

func Test_slicePosition(t *testing.T) {
	tests := []struct {
		needle string
		stack  []string
		pos    int
	}{
		{"foo", []string{"foo", "bar", "baz"}, 0},
		{"baz", []string{"foo", "bar", "baz"}, 2},
		{"fizz", []string{"foo", "bar", "baz"}, -1},
	}

	for _, tt := range tests {
		t.Run(tt.needle, func(t *testing.T) {
			if slicePosition(tt.needle, tt.stack) != tt.pos {
				t.Errorf("needle %s not found at position %d in %s", tt.needle, tt.pos, tt.stack)
			}
		})
	}
}

func Test_anyInSlice(t *testing.T) {
	tests := []struct {
		needle []string
		stack  []string
		in     bool
	}{
		{[]string{"foo", "fizz"}, []string{"foo", "bar", "baz"}, true},
		{[]string{"bar", "baz", "buzz"}, []string{"foo", "bar", "baz"}, true},
		{[]string{"bar", "baz"}, []string{"foo", "bar", "baz"}, true},
		{[]string{"fizz", "buzz"}, []string{"foo", "bar", "baz"}, false},
	}

	for _, tt := range tests {
		t.Run(tt.needle[0], func(t *testing.T) {
			if anyInSlice(tt.needle, tt.stack) != tt.in {
				t.Errorf("any of %s yields %t found in %s", tt.needle, tt.in, tt.stack)
			}
		})
	}
}

func Test_sanitizeTags(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"fizz=buzz,foo=bar", "fizz=buzz,foo=bar"},
		{"foo=bar,fizz=buzz,foo=baz", "fizz=buzz,foo=bar"},
		{"foo=bar,fizz=buzz,foo=baz,", "fizz=buzz,foo=bar"},
		{"foo=bar,fizz=buzz,foo=baz,,", "fizz=buzz,foo=bar"},
		{"host=bar,fizz=buzz,foo=baz,,,", "fizz=buzz,foo=baz,host=bar"},
		{"host=bar,,fizz=buzz,foo=baz", "fizz=buzz,foo=baz,host=bar"},
		{"host=bar,,,,,,,,,,,fizz=buzz,foo=baz", "fizz=buzz,foo=baz,host=bar"},
		{",,,,,,,host=bar,,,,,,,fizz=buzz,,,,,,,,foo=baz,,,,,,,", "fizz=buzz,foo=baz,host=bar"},
		{"foo=baz,host=foo,host=bar,fizz=buzz", "fizz=buzz,foo=baz,host=foo"},
	}

	for _, tt := range tests {
		t.Run(tt.in, func(t *testing.T) {
			got, err := sanitizeTags(tt.in)
			if err != nil {
				t.Errorf("sanitizeTags(%s) returned unexpected error %v", tt.in, err)
			}
			if tt.out != got {
				t.Errorf("sanitizeTags(%s) = %s, wanted %s", tt.in, got, tt.out)
			}
		})
	}
}

func Test_dumpInfluxDB(t *testing.T) {
	tests := []struct {
		name     string
		cfg      influx
		lines    []byte
		expected []byte
	}{
		{
			name: "no_auth, single line",
			cfg: influx{
				URL: "http://example.com",
				DB:  "somedb",
			},
			lines:    []byte("aaa,bbb"),
			expected: []byte("http://example.com/write?db=somedb\naaa,bbb\n"),
		}, {
			name: "auth, single line",
			cfg: influx{
				URL:  "http://example.com",
				User: "foo",
				Pass: "bar",
				DB:   "somedb",
			},
			lines:    []byte("aaa,bbb"),
			expected: []byte("http://example.com/write?db=somedb&u=foo&p=bar\naaa,bbb\n"),
		}, {
			name: "no auth, multi line",
			cfg: influx{
				URL: "http://example.com",
				DB:  "somedb",
			},
			lines:    []byte("aaa,bbb\nccc,ddd"),
			expected: []byte("http://example.com/write?db=somedb\naaa,bbb\nccc,ddd\n"),
		}, {
			name: " auth, multi line",
			cfg: influx{
				URL:  "http://example.com",
				User: "foo",
				Pass: "bar",
				DB:   "somedb",
			},
			lines:    []byte("aaa,bbb\nccc,ddd"),
			expected: []byte("http://example.com/write?db=somedb&u=foo&p=bar\naaa,bbb\nccc,ddd\n"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := bytes.Buffer{}
			w := bufio.NewWriter(&b)
			err := dumpInfluxDB(tt.cfg, tt.lines, w)
			w.Flush()
			if err != nil {
				t.Errorf("dumpInfluxDB(%s) returned unexpected error %v", tt.name, err)
			}
			if !bytes.Equal(b.Bytes(), tt.expected) {
				t.Errorf("dumpInfluxDB(%s) returned \n`%s` but expected \n`%s`", tt.name, b.Bytes(), tt.expected)
			}
		})
	}
}

func Test_point_String(t *testing.T) {
	now := time.Now()

	type fields struct {
		measurement string
		tags        string
		values      map[string]float64
		timestamp   time.Time
	}
	tests := []struct {
		name   string
		fields fields
		want   []string
	}{
		{

			name: "field 1",
			fields: fields{
				measurement: "foo",
				tags:        "bar=baz",
				values:      map[string]float64{"duration": 1.234, "status": 255},
				timestamp:   now,
			},
			want: []string{
				fmt.Sprintf("foo,bar=baz duration=1.234,status=255 %d", now.UnixNano()),
				fmt.Sprintf("foo,bar=baz status=255,duration=1.234 %d", now.UnixNano()),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := point{
				measurement: tt.fields.measurement,
				tags:        tt.fields.tags,
				values:      tt.fields.values,
				timestamp:   tt.fields.timestamp,
			}
			got := p.String()
			ok := false
			for _, s := range tt.want {
				if s == got {
					ok = true
				}
			}
			if !ok {
				t.Errorf("point.String() = %v, want one of %#v", got, tt.want)
			}
		})
	}
}

func Test_batchLogInfluxDB(t *testing.T) {
	tests := []struct {
		name   string
		cfg    influx
		points []point
		want   []string
	}{
		{
			name: "no_auth, single line",
			cfg: influx{
				URL:         "http://example.com",
				DB:          "somedb",
				Measurement: "events",
				BatchSize:   500,
			},
			points: []point{
				{
					tags:      "foo=bar,host=server",
					values:    map[string]float64{"duration": 9.876, "status": 255},
					timestamp: time.Unix(3661000000001, 0),
				},
			},
			want: []string{
				fmt.Sprintf("events,foo=bar,host=server duration=9.876,status=255 %d\n", time.Unix(3661000000001, 0).UnixNano()),
				fmt.Sprintf("events,foo=bar,host=server status=255,duration=9.876 %d\n", time.Unix(3661000000001, 0).UnixNano()),
				fmt.Sprintf("events,host=server,foo=bar status=255,duration=9.876 %d\n", time.Unix(3661000000001, 0).UnixNano()),
				fmt.Sprintf("events,host=server,foo=bar duration=9.876,status=255 %d\n", time.Unix(3661000000001, 0).UnixNano()),
			},
		},
	}

	for _, tt := range tests {
		fn := func(i influx, b []byte, w io.Writer) error {
			if !equalAny(b, tt.want) {
				return fmt.Errorf("got `%s` but expected one in `%+v`", b, tt.want)
			}
			return nil
		}
		err := batchLogInfluxDB(fn, tt.cfg, tt.points, nil)
		if err != nil {
			t.Errorf("batchLogInfluxDB(%s) returned unexpected error %v", tt.name, err)
		}
	}
}

func equalAny(b []byte, s []string) bool {
	for _, t := range s {
		if bytes.Equal(b, []byte(t)) {
			return true
		}
	}

	return false
}

func Test_batchLogInfluxDBChunks(t *testing.T) {
	cfg := influx{
		URL:         "http://example.com",
		DB:          "somedb",
		Measurement: "events",
		BatchSize:   500,
	}
	points := []point{}

	for i := 0; i < 517; i++ {
		points = append(points, point{
			tags:      "foo=bar,host=server",
			values:    map[string]float64{"duration": 9.876, "status": 0},
			timestamp: time.Unix(3661000000001, 0),
		})
	}

	fn := func(i influx, b []byte, w io.Writer) error {
		nl := []byte{'\n'}
		n := bytes.Count(b, nl)

		if n != 500 && n != 17 {
			t.Errorf("batchLogInfluxDB(): we should have either 500 or 17 lines, got %d", n)
		}
		t.Logf("%d", n)
		return nil
	}
	err := batchLogInfluxDB(fn, cfg, points, nil)
	if err != nil {
		t.Errorf("batchLogInfluxDB() returned unexpected error %v", err)
	}
}

func Test_logInfluxDB(t *testing.T) {
	type args struct {
		server influx
		point  point
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "test 1",
			args: args{
				server: influx{
					DB:   "foodb",
					User: "u", Pass: "p",
					Retries: 1,
				},
				point: point{
					measurement: "events",
					tags:        "foo=bar",
					values:      map[string]float64{"duration": 9.876, "status": 0},
				},
			},
			wantErr: false,
		},
	}

	inf := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
		fmt.Fprintln(w, "OK")
	}))
	defer inf.Close()

	for _, tt := range tests {
		tt.args.server.URL = inf.URL
		t.Run(tt.name, func(t *testing.T) {
			if err := logInfluxDB(tt.args.server, []byte(tt.args.point.String()), nil); (err != nil) != tt.wantErr {
				t.Errorf("logInfluxDB() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_executeCommand(t *testing.T) {
	type args struct {
		args    []string
		timeout float64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "ls nowhere",
			args:    args{args: []string{"ls", "/nowhere"}},
			wantErr: true,
		},
		{
			name:    "ls /",
			args:    args{args: []string{"ls", "/"}},
			wantErr: false,
		},
		{
			name: "sleep too long",
			args: args{
				args:    []string{"sleep", "0.2"},
				timeout: 0.1},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cfg := config{command: tt.args.args, Timeout: tt.args.timeout}
			if _, err := executeCommand(cfg, true); (err != nil) != tt.wantErr {
				t.Errorf("executeCommand() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_findProcessAndChildren(t *testing.T) {
	p := findProcessAndChildren(1)

	if len(p) < 2 && os.Getenv("CI") == "" {
		t.Errorf("findProcessAndChildren(1) should return more than 1 process but returned %d", len(p))
	}

	p = findProcessAndChildren(99999)

	if len(p) != 1 {
		t.Errorf("findProcessAndChildren(99999) should return only 1 process but returned %d", len(p))
	}

	if p[0] != 99999 {
		t.Errorf("findProcessAndChildren(99999) should contain 99999 but returned %d", p)
	}
}
