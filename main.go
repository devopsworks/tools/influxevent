package main

import (
	"bufio"
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/crgimenes/goconfig"
	_ "github.com/crgimenes/goconfig/yaml"

	"github.com/struCoder/pidusage"

	ps "github.com/mitchellh/go-ps"
)

type config struct {
	Timeout float64 `yaml:"timeout" cfg:"timeout" cfgDefault:"0" cfgHelper:"command timeout (s); 0 to disable"`
	Period  float64 `yaml:"period" cfg:"period" cfgHelper:"process consumption sample period (ms)"`
	Verbose bool    `yaml:"verbose" cfg:"verbose" cfgHelper:"verbose execution"`
	Influx  influx  `yaml:"influxdb" cfg:"influx" cfgHelper:"influxdb parameters"`
	Lock    lock    `yaml:"lock" cfg:"lock" cfgHelper:"mutex lock file parameters"`
	command []string
}

type lock struct {
	File           string `yaml:"file" cfg:"file" cfgHelper:"mutex lock file"`
	Duration       string `yaml:"duration" cfg:"duration" cfgDefault:"" cfgHelper:"maximum lock duration"`
	ExitValue      int    `yaml:"exit_value" cfg:"exit_value" cfgDefault:"123" cfgHelper:"exit value when lock is not acquired"`
	parsedDuration time.Duration
}

type influx struct {
	DB                 string  `yaml:"db" cfg:"db" cfgHelper:"influxdb database"`
	URL                string  `yaml:"url" cfg:"url" cfgHelper:"influxdb URL"`
	User               string  `yaml:"user" cfg:"user" cfgHelper:"influxdb user"`
	Pass               string  `yaml:"pass" cfg:"pass" cfgHelper:"influxdb password"`
	Measurement        string  `yaml:"measurement" cfg:"measurement" cfgHelper:"influxdb measurement name"`
	Tags               string  `yaml:"tags" cfg:"tags" cfgHelper:"comma-separated influxdb tags (e.g. foo=bar,fizz=buzz)"`
	Retries            int     `yaml:"retries" cfg:"retries" cfgDefault:"3" cfgHelper:"influxdb retries when writing"`
	InsecureSkipVerify bool    `yaml:"skip_verify" cfg:"skip_verify" cfgDefault:"false" cfgHelper:"influxdb insecure skip verify (disable TLS verification)"`
	Timeout            float64 `yaml:"timeout" cfg:"timeout" cfgDefault:"5000" cfgHelper:"influxdb writing timeout (ms)"`
	BatchSize          int     `yaml:"batchsize" cfg:"batchsize" cfgDefault:"600" cfgHelper:"number of metric to batch to influxdb"`
	DryRun             bool    `yaml:"dryrun" cfg:"dryrun" cfgDefault:"false" cfgHelper:"influxdb dry run (runs command but does not log to influxdb)"`
}

type point struct {
	measurement string
	tags        string
	values      map[string]float64
	timestamp   time.Time
}

// Version from git sha1/tags
var Version string

func anyInSlice(needles []string, stack []string) bool {
	for _, needle := range needles {
		if slicePosition(needle, stack) != -1 {
			return true
		}
	}

	return false
}

func slicePosition(needle string, stack []string) int {
	for i, item := range stack {
		if item == needle {
			return i
		}
	}

	return -1
}

func main() {
	if anyInSlice([]string{"-version", "--version"}, os.Args) {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		os.Exit(0)
	}
	cfg := config{}

	// step 3: Pass the instance pointer to the parser
	err := goconfig.Parse(&cfg)
	if err != nil {
		log.Printf("unable to parse arguments: %+v", err)
		return
	}

	if cfg.Influx.Retries <= 0 {
		cfg.Influx.Retries = 3
	}

	if cfg.Influx.BatchSize <= 0 {
		cfg.Influx.BatchSize = 600
	}

	if cfg.Lock.Duration != "" {
		duration, err := time.ParseDuration(cfg.Lock.Duration)
		if err != nil {
			log.Printf("error: unable to parse duration: %v", err)
			os.Exit(1)
		}
		cfg.Lock.parsedDuration = duration
	}

	pos := slicePosition("--", os.Args)
	if pos == -1 || pos > len(os.Args)-2 {
		log.Printf("error: no command specified, use: influxevent [args] -- command...\n")
		os.Exit(1)
	}

	cfg.command = os.Args[slicePosition("--", os.Args)+1:]

	run(cfg)
}

func run(cfg config) {
	// check lock presence
	if cfg.Lock.File != "" {
		err := handleLockFile(cfg)
		if err != nil {
			log.Printf("error handling lockfile: %v", err)
			os.Exit(int(cfg.Lock.ExitValue))
		}
	}

	start := time.Now()
	samples, err := executeCommand(cfg, false)
	end := time.Now()
	duration := end.Sub(start).Seconds()
	exitStatus := 0

	if cfg.Lock.File != "" {
		// remove lock file
		log.Printf("removing lockfile %s", cfg.Lock.File)
		rerr := os.Remove(cfg.Lock.File)
		if rerr != nil {
			log.Printf("unable to remove lockfile %s: %v", cfg.Lock.File, rerr)
		}
	}

	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			// The program has exited with an exit code != 0

			// This works on both Unix and Windows. Although package
			// syscall is generally platform dependent, WaitStatus is
			// defined for both Unix and Windows and in both cases has
			// an ExitStatus() method with the same signature.
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				exitStatus = status.ExitStatus()
			}
		} else {
			log.Fatalf("error running command: %v", err)
		}
	}

	if exitStatus == -1 {
		log.Printf("command has been killed due to timeout")
	}

	if cfg.Influx.URL == "" || cfg.Influx.DB == "" || cfg.Influx.Measurement == "" {
		log.Printf("not writing points to influx since database, url or measurement is not set")
		os.Exit(exitStatus)
	}

	log.Printf("writing points to influx")
	textStatus := "ok"
	if exitStatus != 0 {
		textStatus = "error"
	}

	host, err := os.Hostname()
	if err != nil {
		host = "unknown"
	}

	cfg.Influx.Tags += ",host=" + host + ","

	pt := []point{
		{
			measurement: cfg.Influx.Measurement,
			tags:        "_type=start,_status=" + textStatus,
			values:      map[string]float64{"duration": duration, "status": float64(exitStatus)},
			timestamp:   start,
		}, {

			measurement: cfg.Influx.Measurement,
			tags:        "_type=end,_status=" + textStatus,
			values:      map[string]float64{"duration": duration, "status": float64(exitStatus)},
			timestamp:   end,
		},
	}

	samples = append(samples, pt...)

	if cfg.Influx.DryRun {
		var w io.Writer = os.Stdout
		err = batchLogInfluxDB(dumpInfluxDB, cfg.Influx, samples, w)
	} else {
		err = batchLogInfluxDB(logInfluxDB, cfg.Influx, samples, nil)
	}

	if err != nil {
		log.Printf("unable to write to influxdb: %v", err)
		os.Exit(1)
	}
}

func (p point) String() string {
	influxString := p.measurement
	if p.tags != "" {
		influxString = fmt.Sprintf("%s,%s", influxString, p.tags)
	}

	values := []string{}
	for k, v := range p.values {
		values = append(values, fmt.Sprintf("%s=%g", k, v))
	}
	influxString = fmt.Sprintf("%s %s %d", influxString, strings.Join(values, ","), p.timestamp.UnixNano())

	return influxString
}

// sanitizeTags removes trailing comma & dedups tags
// returns tags sorted by their key (easier for testing !)
func sanitizeTags(t string) (string, error) {
	t = strings.Trim(t, ",")
	elements := strings.Split(t, ",")
	final := map[string]string{}

	for _, e := range elements {
		if e == "" {
			// can happen when two commas are next to each other
			continue
		}
		keyval := strings.Split(e, "=")
		if len(keyval) != 2 {
			return "", fmt.Errorf("invalid format (should be k=v) for tag %s", e)
		}
		if _, ok := final[keyval[0]]; !ok {
			final[keyval[0]] = keyval[1]
		}
	}

	// Sort by key
	keys := make([]string, 0, len(final))
	for k, v := range final {
		keys = append(keys, k+"="+v)
	}
	sort.Strings(keys)

	return strings.Join(keys, ","), nil
}

func batchLogInfluxDB(fn func(influx, []byte, io.Writer) error, cfg influx, points []point, w io.Writer) error {
	var err error
	lines := []byte{}
	for i, pt := range points {
		pt.measurement = cfg.Measurement

		pt.tags, err = sanitizeTags(pt.tags + "," + cfg.Tags)

		if err != nil {
			log.Printf("invalid tags: %v; ignoring user-supplied tags", err)
			// we continue, using only pt.tags
		}

		lines = append(lines, []byte(pt.String())...)
		lines = append(lines, '\n')
		if (i+1)%cfg.BatchSize == 0 {
			err := fn(cfg, lines, w)
			if err != nil {
				return err
			}
			lines = []byte{}
		}
	}

	// Send remainder
	return fn(cfg, lines, w)
}

func dumpInfluxDB(cfg influx, lines []byte, w io.Writer) error {
	var uri string
	if cfg.URL[len(cfg.URL)-1] == '/' {
		uri = fmt.Sprintf("%swrite?db=%s", cfg.URL, cfg.DB)
	} else {
		uri = fmt.Sprintf("%s/write?db=%s", cfg.URL, cfg.DB)
	}

	// Dangerous; shoud use url encoding
	if cfg.User != "" {
		uri += fmt.Sprintf("&u=%s&p=%s", cfg.User, cfg.Pass)
	}

	fmt.Fprintln(w, uri)
	fmt.Fprintln(w, string(lines))
	return nil
}

func logInfluxDB(cfg influx, lines []byte, w io.Writer) error {
	var (
		uri  string
		err  error
		resp *http.Response
	)

	if cfg.URL[len(cfg.URL)-1] == '/' {
		uri = fmt.Sprintf("%swrite?db=%s", cfg.URL, cfg.DB)
	} else {
		uri = fmt.Sprintf("%s/write?db=%s", cfg.URL, cfg.DB)
	}

	// Dangerous; shoud use url encoding
	if cfg.User != "" {
		uri += fmt.Sprintf("&u=%s&p=%s", cfg.User, cfg.Pass)
	}

	tr := &http.Transport{}

	if cfg.InsecureSkipVerify {
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}

	client := &http.Client{
		Timeout:   time.Duration(cfg.Timeout) * time.Millisecond,
		Transport: tr,
	}

	r := bytes.NewReader(lines)

	for try := 0; try < cfg.Retries; try++ {
		resp, err = client.Post(uri, "application/x-www-form-urlencoded", r)
		if err == nil {
			break
		}
	}
	if err != nil {
		return err
	}

	if resp.StatusCode != 204 {
		return fmt.Errorf("unable to write to influxdb server %s, got response: %s", cfg.URL, resp.Status)
	}
	return nil
}

func executeCommand(cfg config, sink bool) ([]point, error) {
	var (
		cmd    *exec.Cmd
		ctx    context.Context
		cancel context.CancelFunc

		wg             sync.WaitGroup
		stdout, stderr io.Writer

		cmderr  error
		samples []point
	)

	done := make(chan struct{}, 2)
	defer close(done)

	if cfg.Timeout != 0 {
		ctx, cancel = context.WithTimeout(context.Background(), time.Duration(cfg.Timeout)*time.Second)
		defer cancel()
		cmd = exec.CommandContext(ctx, cfg.command[0], cfg.command[1:]...)
	} else {
		cmd = exec.Command(cfg.command[0], cfg.command[1:]...)
	}

	cmd.Env = os.Environ()

	sout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	serr, err := cmd.StderrPipe()
	if err != nil {
		return nil, err
	}

	scanOut := bufio.NewScanner(sout)
	scanErr := bufio.NewScanner(serr)

	// By default, we copy to stdout & stderr
	stdout = os.Stdout
	stderr = os.Stderr

	// however, if sink is set, we disable output
	// this is used for tests now
	if sink {
		stdout = io.Discard
		stderr = io.Discard
	}

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	defer close(sigc)

	wg.Add(1)
	go func() {
		defer wg.Done()
		select {
		case <-done:
			log.Printf("subprocess has exited\n")
			return
		case s := <-sigc:
			if cmd.Process == nil {
				log.Printf("received signal %s but no subprocess to forward to\n", s)
				return
			}
			log.Printf("received signal %s, forwarding to subprocess %d\n", s, cmd.Process.Pid)
			err := cmd.Process.Signal(s)
			if err != nil {
				log.Printf("unable to forward signal %s to subprocess %d: %v\n", s, cmd.Process.Pid, err)
			}
			return
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for scanOut.Scan() {
			fmt.Fprintf(stdout, "%s\n", scanOut.Text())
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for scanErr.Scan() {
			fmt.Fprintf(stderr, "%s\n", scanErr.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		done <- struct{}{}
		return nil, err
	}

	if cfg.Verbose {
		log.Printf("started subprocess %d\n", cmd.Process.Pid)
	}

	samplec := make(chan point, 500)

	// Monitor process if a period is specified
	if cfg.Period > 0 {
		wg.Add(1)
		go watch(cmd.Process.Pid, cfg.Period, samplec, &wg)

		for s := range samplec {
			s.tags = "_type=metric," + s.tags
			s.tags = strings.Trim(s.tags, ",")
			samples = append(samples, s)
		}
	}

	go func() {
		cmderr = cmd.Wait()
		// signaling other goroutines
		done <- struct{}{}
	}()

	wg.Wait()

	return samples, cmderr
}

func watch(pid int, period float64, c chan point, wg *sync.WaitGroup) {
	defer wg.Done()
	defer close(c)

	ticker := time.NewTicker(time.Duration(period) * time.Millisecond)

	for {
		t := <-ticker.C

		cpu, mem := 0.0, 0.0

		examine := findProcessAndChildren(pid)
		if len(examine) == 0 {
			return
		}

		for _, p := range findProcessAndChildren(pid) {
			sysInfo, err := pidusage.GetStat(p)
			// so when memoryx usage is zero, we return
			// this is definitively ugly
			if err != nil || sysInfo.Memory == 0 {
				return
			}
			cpu += sysInfo.CPU
			mem += sysInfo.Memory
		}

		c <- point{
			values: map[string]float64{
				"cpu":    cpu,
				"memory": mem,
			},
			timestamp: t,
		}
	}
}

// findProcessAndChildren returns an array containing the requested pid and any
// child of this process.
// The passed PID is always included in the result, event if the process is not
// found.
// As a result, the returned array always contains at least one element
func findProcessAndChildren(pid int) []int {
	processes := []int{pid}
	all, err := ps.Processes()
	if err != nil {
		return processes
	}

	for _, p := range all {
		if p.PPid() == pid {
			processes = append(processes, p.Pid())
		}
	}

	return processes
}

func handleLockFile(cfg config) error {
	log.Printf("using lock file %s", cfg.Lock.File)
	fstat, err := os.Stat(cfg.Lock.File)

	// lock exists but auto lock removal is disabled
	if err == nil && cfg.Lock.parsedDuration == 0 {
		// read pid from file
		pid, err := os.ReadFile(cfg.Lock.File)
		if err != nil {
			return fmt.Errorf("lock file %s present, but unable to read: %v", cfg.Lock.File, err)
		}

		ipid, err := strconv.Atoi(string(pid))
		if err != nil {
			return fmt.Errorf("lock file %s present, but unable to convert PID %s to int: %v", cfg.Lock.File, pid, err)
		}

		proc, err := ps.FindProcess(ipid)
		if proc == nil && err == nil {
			log.Printf("lock file %s present, but owner PID %d is not running; taking lock file ownership", cfg.Lock.File, ipid)
		} else {
			return fmt.Errorf("lock file %s exists and held by pid %s, exiting", cfg.Lock.File, pid)
		}
	}

	if err == nil && cfg.Lock.parsedDuration != 0 {
		// lock exists and auto lock removal is enabled
		if fstat.ModTime().Add(cfg.Lock.parsedDuration).Before(time.Now()) {
			// lock is expired
			log.Printf("taking lock file ownership of expired lock file %s (expired on %s)", cfg.Lock.File, fstat.ModTime().Add(cfg.Lock.parsedDuration))
		} else {
			return fmt.Errorf("unable to run, lock file %s exists and is not expired (expires at %s)", cfg.Lock.File, fstat.ModTime().Add(cfg.Lock.parsedDuration))
		}
	}

	f, err := os.Create(cfg.Lock.File)
	if err != nil {
		return fmt.Errorf("unable to create lock file %s: %v", cfg.Lock.File, err)
	}
	// write current pid to file
	_, err = f.WriteString(fmt.Sprintf("%d", os.Getpid()))
	if err != nil {
		return fmt.Errorf("unable to write pid to lock file %s: %v", cfg.Lock.File, err)
	}

	err = f.Close()
	if err != nil {
		return fmt.Errorf("unable to close lock file %s: %v", cfg.Lock.File, err)
	}
	return nil
}
